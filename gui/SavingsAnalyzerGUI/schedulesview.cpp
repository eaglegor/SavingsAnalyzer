#include "schedulesview.h"
#include "scheduleview.h"
#include "ui_schedulesview.h"

SchedulesView::SchedulesView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::SchedulesView)
{
	ui->setupUi(this);
}

SchedulesView::~SchedulesView()
{
	delete ui;
}

void SchedulesView::update(const MainDataModel &model)
{
	ui->schedulesList->clear();
	int index = 1;
	for(const Schedule &s : model.getScheduleSnapshot().getSchedules())
	{
		ScheduleView* item = new ScheduleView(this);
		item->update(s, model);
		QListWidgetItem* lwitem = new QListWidgetItem();
		lwitem->setSizeHint(QSize(item->sizeHint().width(), item->getDesiredHeight()));
		ui->schedulesList->addItem(lwitem);
		ui->schedulesList->setItemWidget(lwitem, item);

		subwidgets.emplace_back(item);
		items.emplace_back(lwitem);
	}
}
