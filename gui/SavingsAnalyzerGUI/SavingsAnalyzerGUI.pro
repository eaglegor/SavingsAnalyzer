#-------------------------------------------------
#
# Project created by QtCreator 2015-09-02T16:32:35
#
#-------------------------------------------------

QT       += core gui webkit webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SavingsAnalyzerGUI
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    maindatamodel.cpp \
    schedulesnapshot.cpp \
    savingssnapshot.cpp \
    savingstableview.cpp \
    savingsviewtablemodel.cpp \
    schedulesview.cpp \
    performersview.cpp \
    allocationview.cpp \
    scheduleview.cpp \
    performerview.cpp \
    runview.cpp \
    orderoperationdetailsview.cpp \
    timewindowwidget.cpp \
    pickupdetailsview.cpp \
    journeydetailsview.cpp \
    homereturndetailsview.cpp

HEADERS  += mainwindow.h \
    concepts.h \
    maindatamodel.h \
    schedulesnapshot.h \
    savingssnapshot.h \
    savingstableview.h \
    savingsviewtablemodel.h \
    schedulesview.h \
    performersview.h \
    allocationview.h \
    scheduleview.h \
    performerview.h \
    runview.h \
    orderoperationdetailsview.h \
    timewindowwidget.h \
    pickupdetailsview.h \
    journeydetailsview.h \
    homereturndetailsview.h

FORMS    += mainwindow.ui \
    savingstableview.ui \
    schedulesview.ui \
    performersview.ui \
    allocationview.ui \
    scheduleview.ui \
    performerview.ui \
    runview.ui \
    orderoperationdetailsview.ui \
    timewindowwidget.ui \
    pickupdetailsview.ui \
    journeydetailsview.ui \
    homereturndetailsview.ui

QMAKE_CXXFLAGS += -std=c++0x
