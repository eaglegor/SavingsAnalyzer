#include "pickupdetailsview.h"
#include "ui_pickupdetailsview.h"

PickupDetailsView::PickupDetailsView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::PickupDetailsView)
{
	ui->setupUi(this);
}

PickupDetailsView::~PickupDetailsView()
{
	delete ui;
}

void PickupDetailsView::update(const Allocation &allocation, const MainDataModel &model)
{
	ui->pickupAddress->setText(allocation.startPoint.name);
}
