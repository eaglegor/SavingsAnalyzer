#ifndef SAVINGSSNAPSHOT_H
#define SAVINGSSNAPSHOT_H

#include "concepts.h"
#include <QFile>
#include <unordered_map>

class SavingsSnapshot
{
	public:
		SavingsSnapshot();

		void load(QFile &file, std::unordered_map<QString, GeoPoint> &geopoints);

		const std::vector<Saving>& getSavings() const;

	private:
		std::vector<Saving> savings;
};

#endif // SAVINGSSNAPSHOT_H
