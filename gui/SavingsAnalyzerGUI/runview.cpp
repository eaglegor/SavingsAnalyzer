#include "allocationview.h"
#include "runview.h"
#include "timewindowwidget.h"
#include "ui_runview.h"

RunView::RunView(int id, QWidget *parent) :
	QWidget(parent),
	id(id),
	ui(new Ui::RunView)
{
	ui->setupUi(this);
}

RunView::~RunView()
{
	delete ui;
}

void RunView::update(const Run &run, const MainDataModel &model)
{
	ui->runIndexLabel->setText("Run " + QString::number(id));

	ui->startPointLabel->setText(run.startPoint.name);
	ui->endPointLabel->setText(run.endPoint.name);

	TimeWindowWidget* tw = new TimeWindowWidget(ui->timeWindowWidget);
	ui->timeWindowWidget->layout()->addWidget(tw);
	tw->setBounds(run.start_time, run.end_time);
	allocation_time_window.reset(tw);

	ui->allocationsList->clear();

	desiredHeight = 0;

	for(const Allocation &a : run.allocations)
	{
		AllocationView* item = new AllocationView(this);
		item->update(a, model);
		QListWidgetItem* lwitem = new QListWidgetItem();
		lwitem->setSizeHint(item->sizeHint());
		ui->allocationsList->addItem(lwitem);
		ui->allocationsList->setItemWidget(lwitem, item);

		desiredHeight+=item->sizeHint().height() * 1.01;

		subWidgets.emplace_back(item);
		items.emplace_back(lwitem);
	}
}

int RunView::getDesiredHeight()
{
	return desiredHeight;
}
