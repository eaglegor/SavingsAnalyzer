#ifndef TIMEWINDOWWIDGET_H
#define TIMEWINDOWWIDGET_H

#include <QWidget>

namespace Ui {
	class TimeWindowWidget;
}

class TimeWindowWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit TimeWindowWidget(QWidget *parent = 0);
		~TimeWindowWidget();

		void setBounds(qulonglong left, qulonglong right);

	private:
		Ui::TimeWindowWidget *ui;
};

#endif // TIMEWINDOWWIDGET_H
