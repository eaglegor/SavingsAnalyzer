#include "orderoperationdetailsview.h"
#include "ui_orderoperationdetailsview.h"

OrderOperationDetailsView::OrderOperationDetailsView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::OrderOperationDetailsView)
{
	ui->setupUi(this);
}

OrderOperationDetailsView::~OrderOperationDetailsView()
{
	delete ui;
}

void OrderOperationDetailsView::update(const Allocation &allocation, const MainDataModel &model)
{
	const Order &order = model.getOrders().at(allocation.order_reference);
	setOrderReference(allocation.order_reference);
	setTimeWindow(order.window_start, order.window_end);
	setAddress(allocation.startPoint.name);
	setOperationType(order.type);
}

void OrderOperationDetailsView::setOrderReference(const QString &reference)
{
	ui->orderReferenceLabel->setText(reference);
}

void OrderOperationDetailsView::setTimeWindow(qulonglong left, qulonglong right)
{
	tw_widget.reset(new TimeWindowWidget(ui->timeWindowPlace));
	tw_widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	ui->timeWindowPlace->layout()->addWidget(tw_widget.data());
	tw_widget->setBounds(left, right);
}

void OrderOperationDetailsView::setAddress(const QString &address)
{
	ui->addressLabel->setText(address);
}

void OrderOperationDetailsView::setOperationType(const QString &type)
{
	ui->operationType->setText(type);
}
