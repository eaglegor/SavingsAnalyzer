#ifndef PERFORMERSVIEW_H
#define PERFORMERSVIEW_H

#include <QWidget>
#include <QCheckBox>
#include <QListWidgetItem>
#include "maindatamodel.h"
#include "performerview.h"

namespace Ui {
	class PerformersView;
}

class PerformersView : public QWidget
{
		Q_OBJECT

	public:
		explicit PerformersView(QWidget *parent = 0);
		~PerformersView();

		void update(const MainDataModel &model);

	public slots:
		void onPerformerRouteVisibilityChanged(int id, bool isVisible)
		{
			emit performerRouteVisibilityChanged(id, isVisible);
		}

		void onPerformerOrdersVisibilityChanged(int id, bool isVisible)
		{
			emit performerOrdersVisibilityChanged(id, isVisible);
		}

		void onShowScheduleRequest(int id)
		{
			emit showScheduleRequest(id);
		}

	signals:
		void performerRouteVisibilityChanged(int id, bool isVisible);
		void performerOrdersVisibilityChanged(int id, bool isVisible);
		void showScheduleRequest(int id);

	private:
		std::vector<QListWidgetItem*> items;
		std::vector<PerformerView*> checkboxes;
		Ui::PerformersView *ui;
};

#endif // PERFORMERSVIEW_H
