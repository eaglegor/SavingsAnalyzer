#ifndef SAVINGSVIEWTABLEMODEL_H
#define SAVINGSVIEWTABLEMODEL_H

#include <QAbstractTableModel>
#include "maindatamodel.h"

class SavingsViewTableModel : public QAbstractTableModel
{
	public:
		SavingsViewTableModel(MainDataModel &model);


	signals:

	public slots:

		// QAbstractItemModel interface
	public:
		virtual int rowCount(const QModelIndex &parent) const;
		virtual int columnCount(const QModelIndex &parent) const;
		virtual QVariant data(const QModelIndex &index, int role) const;
		virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
		void applyOrderReferenceFilter(const QString& filterString);
		void toggleBreakpoint(int row);
		void clearBreakpoints();

	signals:
		void savingBreakPointOn(const Saving &s);
		void savingBreakPointOff(const Saving &s);

	private:
		int getSavingIndex(int row) const
		{
			if(filtered_saving_row_mapping.empty()) return row;
			else if(row < filtered_saving_row_mapping.size()) return filtered_saving_row_mapping[row];
			else return -1;
		}

		MainDataModel &model;

		std::vector<int> filtered_saving_row_mapping;
};

#endif // SAVINGSVIEWTABLEMODEL_H
