{
	"planned_jobs": 1,
	"used_vehicles": 1,
	"schedules":
	[
		{
			"performer_id": 0,
			"runs": [
				{
					"start_time": 456,
					"end_time": 435,
					"start_point": "DC1_position",
					"end_point": "DC1_position",
					"allocations":
					[
						{
							"type": "Journey",
							"duration": 677,
							"startPoint": "DC1_position",
							"endPoint": "Order1_position"
						},
						{
							"type": "Operation",
							"order_ref": "Order1",
							"actual_start_time": 4567,
							"actual_end_time": 6789
						},
						{
							"type": "Journey",
							"duration": 650,
							"startPoint": "Order1_position",
							"endPoint": "DC1_position"
						}
					]
				}
			]
		},
		{
			"performer_id": 1,
			"runs": []
		}
	]
}