#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "maindatamodel.h"
#include "schedulesnapshot.h"
#include "savingssnapshot.h"
#include "savingstableview.h"
#include "performersview.h"
#include "schedulesview.h"
#include "scheduleview.h"
#include <unordered_set>

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget *parent = 0);
		~MainWindow();

	private slots:
		void on_fitMapButton_clicked();

		void on_actionLoadDump_triggered();

		void on_nextStepButton_clicked();

		void on_prevStepButton_clicked();

		void on_currentStepSlider_valueChanged(int value);

		void on_nextSuccStepButton_clicked();

		void on_prevSuccStepButton_clicked();

		void on_actionShow_savings_table_triggered();

		void nextFrame();

		void on_actionShow_vehicles_triggered();

		void onPerformerRoutesVisibilityChanged(int id, bool isVisible);
		void onPerformerOrdersVisibilityChanged(int id, bool isVisible);


		void on_drawCurrentSaving_stateChanged(int arg1);

		void on_autoplay_stateChanged(int arg1);

		void on_actionShow_schedules_list_triggered();

		void onShowScheduleRequest(int id);

		void onBreakPointReached();

	private:

		void drawRESPs();
		void drawOrders();
		void drawJourneys();
		void updatePlannedJobs();
		QString formatSavingString(const int index, const int count, const Saving &s);

		void setCurrentStep(int step);

		Ui::MainWindow *ui;
		MainDataModel model;
		ScheduleSnapshot schedule_snapshot;
		SavingsSnapshot savings_snapshot;

		SavingsTableView* savings_table_view;
		SavingsViewTableModel* savings_table_model;

		QTimer *timer;

		PerformersView* performersView;

		std::unordered_set<int> route_disabled_performers;
		std::unordered_set<int> orders_disabled_performers;

		SchedulesView* schedules_view;

		std::vector<ScheduleView*> schedule_views;

		bool drawCurrentSaving;

};

#endif // MAINWINDOW_H
