#ifndef ORDEROPERATIONDETAILSVIEW_H
#define ORDEROPERATIONDETAILSVIEW_H

#include "maindatamodel.h"
#include "timewindowwidget.h"

#include <QWidget>

namespace Ui {
	class OrderOperationDetailsView;
}

class OrderOperationDetailsView : public QWidget
{
		Q_OBJECT

	public:
		explicit OrderOperationDetailsView(QWidget *parent = 0);
		~OrderOperationDetailsView();

		void update(const Allocation &allocation, const MainDataModel& model);

	private:
		void setOrderReference(const QString &reference);
		void setTimeWindow(qulonglong left, qulonglong right);
		void setAddress(const QString &address);
		void setOperationType(const QString &type);

		QSharedPointer<TimeWindowWidget> tw_widget;
		Ui::OrderOperationDetailsView *ui;
};

#endif // ORDEROPERATIONDETAILSVIEW_H
