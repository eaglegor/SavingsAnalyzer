#include "savingsviewtablemodel.h"
#include <unordered_set>
#include <iostream>

SavingsViewTableModel::SavingsViewTableModel(MainDataModel &model):
model(model)
{

}

int SavingsViewTableModel::rowCount(const QModelIndex &parent) const
{
	if(filtered_saving_row_mapping.empty())
	{
		return model.getSavingsSnapshot().getSavings().size();
	}
	else
	{
		return filtered_saving_row_mapping.size();
	}
}

int SavingsViewTableModel::columnCount(const QModelIndex &parent) const
{
	return 8;
}

QVariant SavingsViewTableModel::data(const QModelIndex &index, int role) const
{
	if(role == Qt::DisplayRole)
	{
		int actual_row;
		if(filtered_saving_row_mapping.empty()) actual_row = index.row();
		else actual_row = filtered_saving_row_mapping[index.row()];

		const Saving &s = model.getSavingsSnapshot().getSavings()[actual_row];
		switch(index.column())
		{
			case 0: return QVariant::fromValue(QString::number(s.i) + QString(" [") + QString(model.getOrderReference(s.i)) + QString("]"));
			case 1: return QVariant::fromValue(QString::number(s.j) + QString(" [") + QString(model.getOrderReference(s.j)) + QString("]"));
			case 2: return QVariant::fromValue(s.priority);
			case 3: return QVariant::fromValue(s.value);
			case 4: return QVariant::fromValue(QString(s.type));
			case 5: return QVariant::fromValue(QString(s.respStart.name));
			case 6: return QVariant::fromValue(QString(s.respEnd.name));
		}
	}
	if(role == Qt::BackgroundColorRole)
	{
		int actual_row;
		if(filtered_saving_row_mapping.empty()) actual_row = index.row();
		else actual_row = filtered_saving_row_mapping[index.row()];

		if(model.getCurrentSavingIndex() == actual_row)
		{
			return QColor::fromRgb(230, 230, 255);
		}
		else if(model.hasSavingBreakPoint( model.getSavingsSnapshot().getSavings()[getSavingIndex(index.row())]))
		{
			return QColor::fromRgb(255, 230, 230);
		}
		else
		{
			return QColor::fromRgb(255, 255, 255);
		}
	}
	return QVariant();
}

QVariant SavingsViewTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		switch (section) {
			case 0: return "i [Reference]";
			case 1: return "j [Reference]";
			case 2: return "p";
			case 3: return "v";
			case 4: return "t";
			case 5: return "sp";
			case 6: return "ep";
		}
	}
	if(role == Qt::DisplayRole && orientation == Qt::Vertical)
	{
		if(filtered_saving_row_mapping.empty()) return section;
		else if(section < filtered_saving_row_mapping.size()) return filtered_saving_row_mapping[section];
	}
	return QVariant();
}

void SavingsViewTableModel::applyOrderReferenceFilter(const QString &filterString)
{
	filtered_saving_row_mapping.clear();

	QStringList str_references = filterString.split(',');

	std::unordered_set<QString> references;

	for(QString str: str_references)
	{
		references.emplace(str.trimmed());
	}

	for(int i = 0; i < model.getSavingsSnapshot().getSavings().size(); ++i)
	{
		const Saving &s = model.getSavingsSnapshot().getSavings()[i];
		if(references.find(QString(model.getOrderReference(s.i))) != references.end() || references.find(QString(model.getOrderReference(s.j))) != references.end())
		{
			filtered_saving_row_mapping.emplace_back(i);
		}
		if(references.find(model.getOrderReference(s.i) + QString("&") + model.getOrderReference(s.j)) != references.end())
		{
			filtered_saving_row_mapping.emplace_back(i);
		}
		if(references.find(model.getOrderReference(s.j) + QString("&") + model.getOrderReference(s.i)) != references.end())
		{
			filtered_saving_row_mapping.emplace_back(i);
		}
	}
}

void SavingsViewTableModel::toggleBreakpoint(int row)
{
	const Saving &s = model.getSavingsSnapshot().getSavings()[getSavingIndex(row)];
	if(!model.hasSavingBreakPoint(s))
	{
		model.addSavingBreakPoint(s);
	}
	else model.removeSavingBreakPoint(s);
}

void SavingsViewTableModel::clearBreakpoints()
{
	model.clearBreakPoints();
}
