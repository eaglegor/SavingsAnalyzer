#include "journeydetailsview.h"
#include "ui_journeydetailsview.h"
#include <QDateTime>
#include <iostream>

JourneyDetailsView::JourneyDetailsView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::JourneyDetailsView)
{
	ui->setupUi(this);
}

JourneyDetailsView::~JourneyDetailsView()
{
	delete ui;
}

void JourneyDetailsView::update(const Allocation &allocation, const MainDataModel &model)
{
	QDateTime diff = QDateTime::fromMSecsSinceEpoch(allocation.endTime - allocation.startTime).toUTC();

	ui->journeyDuration->setText((diff.date().day() > 1 ? QString::number(diff.date().day() - 1) + "days, " : "") + diff.time().toString("hh:mm:ss"));
}
