#include "schedulesnapshot.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDataStream>

ScheduleSnapshot::ScheduleSnapshot()
{

}

Allocation loadAllocation(const QJsonObject &root, std::unordered_map<QString, GeoPoint> &geopoints)
{
	Allocation allocation;

	allocation.order_reference = root["orderReference"].toString();
	allocation.startTime =  root["startTime"].toString().toULongLong();
	allocation.endTime = root["endTime"].toString().toULongLong();
	allocation.startPoint = geopoints[ root["startPoint"].toString() ];
	allocation.endPoint = geopoints[ root["endPoint"].toString() ];
	allocation.type = root["type"].toString();

	QJsonArray breaks = root["breaks"].toArray();

	for(const QJsonValueRef &v : breaks)
	{
		BreakTime bt;
		bt.startTime = v.toObject()["startTime"].toString().toULongLong();
		bt.endTime = v.toObject()["endTime"].toString().toULongLong();
		allocation.breaks.push_back(bt);
	}

	return allocation;
}

Run loadRun(const QJsonObject &root, std::unordered_map<QString, GeoPoint> &geopoints)
{
	Run run;

	run.start_time = root["start_time"].toString().toULongLong();
	run.end_time = root["end_time"].toString().toULongLong();
	run.startPoint = geopoints[ root["start_point"].toString()];
	run.endPoint = geopoints[ root["end_point"].toString()];

	QJsonArray allocations_array = root["allocations"].toArray();

	for(const QJsonValueRef &v : allocations_array)
	{
		Allocation allocation = loadAllocation(v.toObject(), geopoints);
		run.allocations.push_back(allocation);
	}

	return run;
}

Schedule loadSchedule(const QJsonObject &root, std::unordered_map<QString, GeoPoint> &geopoints)
{
	Schedule schedule;

	schedule.performer_id = root["performer_id"].toInt();

	QJsonArray runs_array = root["runs"].toArray();

	for(const QJsonValueRef &v : runs_array)
	{
		Run run = loadRun(v.toObject(), geopoints);
		schedule.runs.push_back(run);
	}

	return schedule;
}

void ScheduleSnapshot::load(QFile &file, std::unordered_map<QString, GeoPoint> &geopoints)
{
	schedules.clear();

	QString val;
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	val = file.readAll();
	file.close();

	QJsonParseError err;

	QJsonDocument d = QJsonDocument::fromJson(val.toUtf8(), &err);
	QJsonObject root = d.object();

	planned_jobs = root["planned_jobs"].toInt();
	used_vehicles = root["used_vehicles"].toInt();

	QJsonValue val2 = root["schedules"];

	QJsonArray schedules_array = val2.toArray();

	for(const QJsonValueRef &v : schedules_array)
	{
		Schedule schedule = loadSchedule(v.toObject(), geopoints);
		schedules.push_back(schedule);
	}
}

size_t ScheduleSnapshot::getPlannedJobs() const
{
	return planned_jobs;
}

size_t ScheduleSnapshot::getUsedVehicles() const
{
	return used_vehicles;
}

const std::vector<Schedule>& ScheduleSnapshot::getSchedules() const
{
	return schedules;
}

