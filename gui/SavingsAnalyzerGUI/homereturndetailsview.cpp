#include "homereturndetailsview.h"
#include "ui_homereturndetailsview.h"

HomeReturnDetailsView::HomeReturnDetailsView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::HomeReturnDetailsView)
{
	ui->setupUi(this);
}

HomeReturnDetailsView::~HomeReturnDetailsView()
{
	delete ui;
}

void HomeReturnDetailsView::update(const Allocation &allocation, const MainDataModel &model)
{
	ui->returnAddress->setText(allocation.startPoint.name);
}
