#ifndef JOURNEYDETAILSVIEW_H
#define JOURNEYDETAILSVIEW_H

#include "concepts.h"
#include "maindatamodel.h"

#include <QWidget>

namespace Ui {
	class JourneyDetailsView;
}

class JourneyDetailsView : public QWidget
{
		Q_OBJECT

	public:
		explicit JourneyDetailsView(QWidget *parent = 0);
		~JourneyDetailsView();

		void update(const Allocation& allocation, const MainDataModel& model);

	private:
		Ui::JourneyDetailsView *ui;
};

#endif // JOURNEYDETAILSVIEW_H
