#include "savingstableview.h"
#include "ui_savingstableview.h"
#include <iostream>

SavingsTableView::SavingsTableView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::SavingsTableView),
	model(nullptr)
{
	ui->setupUi(this);
	ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

SavingsTableView::~SavingsTableView()
{
	delete ui;
}

void SavingsTableView::setTableModel(SavingsViewTableModel *model)
{
	ui->tableView->setModel(model);
	this->model = model;
}

void SavingsTableView::setCurrentSaving(int index)
{
	ui->tableView->reset();
	ui->tableView->scrollTo(ui->tableView->model()->index(index, 0));
}

void SavingsTableView::on_applyFilter_clicked()
{
	model->applyOrderReferenceFilter(ui->orderReferencesFilter->text());
	ui->tableView->reset();
	//ui->tableView->scrollTo(ui->tableView->model()->index(index, 0));
}

void SavingsTableView::on_tableView_doubleClicked(const QModelIndex &index)
{
	//std::cout << "Breakpoint toggle: " << index.row() << std::endl;
	model->toggleBreakpoint(index.row());
	ui->tableView->reset();
}

void SavingsTableView::on_pushButton_clicked()
{
	model->clearBreakpoints();
	ui->tableView->reset();
}
