#include "maindatamodel.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QTextStream>
#include <iostream>
#include <stdlib.h>
#include <time.h>

MainDataModel::MainDataModel()
{

}

void MainDataModel::setCurrentStep(long step_number)
{
	QFile savingsfile(saving_snapshot_filenames[step_number]);
	current_savings_snapshot.load(savingsfile, geopoints);

	QFile schedulefile(schedule_snapshot_filenames[step_number]);
	current_schedule_snapshot.load(schedulefile, geopoints);

	current_step = step_number;
}

void MainDataModel::load(QDir dump_folder)
{
	srand(time(nullptr));

	dump_root = dump_folder;

	savings_index.clear();
	geopoints.clear();
	orders.clear();
	performers.clear();
	dcs.clear();
	order_reference_index.clear();

	saving_snapshot_filenames.clear();
	schedule_snapshot_filenames.clear();

	QFile gpfile(dump_folder.absoluteFilePath("geopoints.txt"));
	loadGeopoints(gpfile);

	QFile ordfile(dump_folder.absoluteFilePath("orders.txt"));
	loadOrders(ordfile);

	QFile pffile(dump_folder.absoluteFilePath("performers.txt"));
	loadPerformers(pffile);

	QFile dcfile(dump_folder.absoluteFilePath("distribution_centers.txt"));
	loadDCs(dcfile);

	QFile sifile(dump_folder.absoluteFilePath("SavingsIndices.txt"));
	loadSavingIndex(sifile);



	fillSavingSnapshotFiles();
	fillScheduleSnapshotFiles();

	setCurrentStep(0);
}

int MainDataModel::getStepsCount()
{
	return savings_index.size();
}

int MainDataModel::getCurrentStep()
{
	return current_step;
}

long MainDataModel::getCurrentSavingIndex() const
{
	return savings_index[current_step];
}

const std::unordered_map<QString, Order> &MainDataModel::getOrders() const
{
	return orders;
}

const std::unordered_map<int, Performer> &MainDataModel::getPerformers() const
{
	return performers;
}

const std::unordered_map<QString, GeoPoint> &MainDataModel::getGeopoints()
{
	return geopoints;
}

const std::unordered_map<QString, DC> &MainDataModel::getDCs()
{
	return dcs;
}

const SavingsSnapshot &MainDataModel::getSavingsSnapshot() const
{
	return current_savings_snapshot;
}

const ScheduleSnapshot &MainDataModel::getScheduleSnapshot() const
{
	return current_schedule_snapshot;
}

int MainDataModel::getNextSuccessfulStep() const
{
	for(int i = current_step + 1; i < savings_index.size(); ++i)
	{
		QString file_name = schedule_snapshot_filenames[i];
		QFileInfo info(file_name);
		if(i == info.baseName().toInt())
		{
			return i;
		}

		if(hasSavingBreakPoint(current_savings_snapshot.getSavings()[savings_index[i]]))
		{
			emit breakPointReached();
			return i;
		}
	}
	return savings_index.size() - 1;
}

int MainDataModel::getPrevSuccessfulStep() const
{
	for(int i = current_step - 1; i >= 0; --i)
	{
		QString file_name = schedule_snapshot_filenames[i];
		QFileInfo info(file_name);
		if(i == info.baseName().toInt())
		{
			return i;
		}

		if(hasSavingBreakPoint(current_savings_snapshot.getSavings()[savings_index[i]]))
		{
			emit breakPointReached();
			return i;
		}
	}
	return 0;
}

QString MainDataModel::getOrderReference(int index) const
{
	return order_reference_index[index];
}

void MainDataModel::addSavingBreakPoint(const Saving &s)
{
	if(!hasSavingBreakPoint(s))
	{
		breakpoints.emplace_back(s);
	}
}

void MainDataModel::removeSavingBreakPoint(const Saving &s)
{
	if(hasSavingBreakPoint(s))
	{
		breakpoints.erase(std::find(breakpoints.begin(), breakpoints.end(), s));
	}
}

bool MainDataModel::hasSavingBreakPoint(const Saving &s)  const
{
	return std::find(breakpoints.begin(), breakpoints.end(), s) != breakpoints.end();
}

void MainDataModel::clearBreakPoints()
{
	breakpoints.clear();
}

void MainDataModel::loadOrders(QFile &file)
{
	QString val;
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	val = file.readAll();
	file.close();

	QJsonParseError err;

	QJsonDocument d = QJsonDocument::fromJson(val.toUtf8(), &err);
	QJsonObject root = d.object();

	QJsonValue val2 = root["orders"];

	QJsonArray orders_array = val2.toArray();

	for(const QJsonValueRef &v : orders_array)
	{
		Order order;
		order.reference = v.toObject()["reference"].toString();
		order.dc_name = v.toObject()["dc"].toString();
		order.window_start = v.toObject()["window_start"].toString().toULongLong();
		order.window_end = v.toObject()["window_end"].toString().toULongLong();
		order.type = v.toObject()["type"].toString();
		order.location = geopoints[v.toObject()["location"].toString()];
		orders.emplace(order.reference, order);

		order_reference_index.push_back(order.reference);
	}

}

void MainDataModel::loadPerformers(QFile &file)
{
	QString val;
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	val = file.readAll();
	file.close();

	QJsonParseError err;

	QJsonDocument d = QJsonDocument::fromJson(val.toUtf8(), &err);
	QJsonObject root = d.object();

	QJsonValue val2 = root["performers"];

	QJsonArray performers_array = val2.toArray();

	for(const QJsonValueRef &v : performers_array)
	{
		Performer performer;
		performer.id = v.toObject()["id"].toInt();
		performer.resource_name = v.toObject()["resource_name"].toString();
		performer.dc_name = v.toObject()["dc"].toString();
		performer.start_time = v.toObject()["start_time"].toString().toULongLong();
		performer.end_time = v.toObject()["end_time"].toString().toULongLong();
		performer.driving_time_limit = v.toObject()["driving_time_limit"].toInt();
		performer.duty_time_limit = v.toObject()["duty_time_limit"].toInt();
		performer.startPoint = geopoints[v.toObject()["startPointLocation"].toString()];
		performer.dcPoint = geopoints[v.toObject()["depotLocation"].toString()];
		performer.endPoint = geopoints[v.toObject()["endPointLocation"].toString()];

		int r = rand();
		int g = rand();
		int b = rand();

		performer.color = QColor::fromRgb(r % 200, g % 200, b % 200);

		performers.emplace(performer.id, performer);
	}
}

void MainDataModel::loadGeopoints(QFile &file)
{
	QString val;
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	val = file.readAll();
	file.close();

	QJsonParseError err;

	QJsonDocument d = QJsonDocument::fromJson(val.toUtf8(), &err);
	QJsonObject root = d.object();

	QJsonValue val2 = root["geopoints"];

	QJsonArray points = val2.toArray();

	for(const QJsonValueRef &v : points)
	{
		GeoPoint point;
		point.name = v.toObject()["name"].toString();
		point.latitude = v.toObject()["latitude"].toDouble();
		point.longitude = v.toObject()["longitude"].toDouble();
		point.address = v.toObject()["address"].toString();
		geopoints.emplace(point.name, point);
	}
}

void MainDataModel::loadDCs(QFile &file)
{
	QString val;
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	val = file.readAll();
	file.close();

	QJsonParseError err;

	QJsonDocument d = QJsonDocument::fromJson(val.toUtf8(), &err);
	QJsonObject root = d.object();

	QJsonValue val2 = root["distribution_centers"];

	QJsonArray dc_array = val2.toArray();

	for(const QJsonValueRef &v : dc_array)
	{
		DC dc;
		dc.name = v.toObject()["name"].toString();
		dc.location = geopoints[v.toObject()["location"].toString()];
		dcs.emplace(dc.name, dc);
	}
}

void MainDataModel::loadSavingIndex(QFile &file)
{
	file.open(QIODevice::ReadOnly | QIODevice::Text);

	QTextStream in(&file);


	while(!in.atEnd())
	{
		qint32 index;
		in >> index;
		savings_index.push_back(index);
	}
}

void MainDataModel::fillSavingSnapshotFiles()
{
	saving_snapshot_filenames.resize(getStepsCount());
	QDir savingsSnapshotRoot = dump_root;
	savingsSnapshotRoot.cd("SavingsDumps");
	QFileInfoList contents = savingsSnapshotRoot.entryInfoList();

	for(QFileInfo& info: contents)
	{
		if(info.baseName() != ".." && info.baseName() != ".")
		{
			long step_id = info.baseName().toLong();
			saving_snapshot_filenames[step_id] = info.absoluteFilePath();
		}
	}

	QString current_filename = "";
	for(int i = 0; i < saving_snapshot_filenames.size(); ++i)
	{
		if(saving_snapshot_filenames[i] != "")
		{
			current_filename = saving_snapshot_filenames[i];
		}
		else
		{
			saving_snapshot_filenames[i] = current_filename;
		}
	}
}

void MainDataModel::fillScheduleSnapshotFiles()
{
	schedule_snapshot_filenames.resize(getStepsCount());
	QDir savingsSnapshotRoot = dump_root;
	savingsSnapshotRoot.cd("SchedulesDumps");
	QFileInfoList contents = savingsSnapshotRoot.entryInfoList();

	for(QFileInfo& info: contents)
	{
		long step_id = info.baseName().toLong();
		schedule_snapshot_filenames[step_id] = info.absoluteFilePath();
	}

	QString current_filename = "";
	for(int i = 0; i < schedule_snapshot_filenames.size(); ++i)
	{
		if(schedule_snapshot_filenames[i] != "")
		{
			current_filename = schedule_snapshot_filenames[i];
		}
		else
		{
			schedule_snapshot_filenames[i] = current_filename;
		}
	}
}



