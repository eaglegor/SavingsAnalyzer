#ifndef SCHEDULESVIEW_H
#define SCHEDULESVIEW_H

#include "maindatamodel.h"

#include <QListWidgetItem>
#include <QWidget>

namespace Ui {
	class SchedulesView;
}

class SchedulesView : public QWidget
{
		Q_OBJECT

	public:
		explicit SchedulesView(QWidget *parent = 0);
		~SchedulesView();

		void update(const MainDataModel &model);

	private:
		Ui::SchedulesView *ui;

		std::vector<QWidget*> subwidgets;
		std::vector<QListWidgetItem*> items;
};

#endif // SCHEDULESVIEW_H
