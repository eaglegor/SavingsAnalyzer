#ifndef SAVINGSTABLEVIEW_H
#define SAVINGSTABLEVIEW_H

#include "savingsviewtablemodel.h"

#include <QScrollArea>
#include <QAbstractTableModel>

namespace Ui {
	class SavingsTableView;
}

class SavingsTableView : public QWidget
{
		Q_OBJECT

	public:
		explicit SavingsTableView(QWidget *parent = 0);
		~SavingsTableView();

		void setTableModel(SavingsViewTableModel* model);
		void setCurrentSaving(int index);

	private slots:
		void on_applyFilter_clicked();

		void on_tableView_doubleClicked(const QModelIndex &index);

		void on_pushButton_clicked();

	signals:
		void savingBreakPointOn(const Saving &s);
		void savingBreakPointOff(const Saving &s);

	public slots:
		void onSavingBreakPointOn(const Saving &s)
		{
			emit savingBreakPointOn(s);
		}

		void onSavingBreakPointOff(const Saving &s)
		{
			emit savingBreakPointOff(s);
		}

	private:
		Ui::SavingsTableView *ui;
		SavingsViewTableModel* model;
};

#endif // SAVINGSTABLEVIEW_H
