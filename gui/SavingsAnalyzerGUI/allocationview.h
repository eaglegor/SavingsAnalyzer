#ifndef ALLOCATIONVIEW_H
#define ALLOCATIONVIEW_H

#include "concepts.h"
#include "maindatamodel.h"
#include <QWidget>

namespace Ui {
	class AllocationView;
}

class AllocationView : public QWidget
{
		Q_OBJECT

	public:
		explicit AllocationView(QWidget *parent = 0);
		~AllocationView();

		void update(const Allocation &allocation, const MainDataModel &model);

	private:
		void setAllocationType(const Allocation& allocation);

		QSharedPointer<QWidget> allocation_time_window;
		QSharedPointer<QWidget> allocation_details;
		Ui::AllocationView *ui;
};

#endif // ALLOCATIONVIEW_H
