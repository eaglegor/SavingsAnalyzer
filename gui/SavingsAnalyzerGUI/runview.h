#ifndef RUNVIEW_H
#define RUNVIEW_H

#include "concepts.h"
#include "maindatamodel.h"

#include <QListWidgetItem>
#include <QWidget>

namespace Ui {
	class RunView;
}

class RunView : public QWidget
{
		Q_OBJECT

	public:
		explicit RunView(int id, QWidget *parent = 0);
		~RunView();

		void update(const Run &run, const MainDataModel &model);

		int getDesiredHeight();

	private:
		int id;
		Ui::RunView *ui;

		QSharedPointer<QWidget> allocation_time_window;

		std::vector<QWidget*> subWidgets;
		std::vector<QListWidgetItem*> items;

		int desiredHeight;

};

#endif // RUNVIEW_H
