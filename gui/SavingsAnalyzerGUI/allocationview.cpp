#include "allocationview.h"
#include "homereturndetailsview.h"
#include "journeydetailsview.h"
#include "orderoperationdetailsview.h"
#include "pickupdetailsview.h"
#include "ui_allocationview.h"

AllocationView::AllocationView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::AllocationView)
{
	ui->setupUi(this);
}

AllocationView::~AllocationView()
{
	delete ui;
}

void AllocationView::update(const Allocation &allocation, const MainDataModel& model)
{
	TimeWindowWidget* tw = new TimeWindowWidget(ui->planningTimeFrame);
	ui->planningTimeFrame->layout()->addWidget(tw);
	tw->setBounds(allocation.startTime, allocation.endTime);
	allocation_time_window.reset(tw);
	setAllocationType(allocation);

	if(allocation.type == "O")
	{
		OrderOperationDetailsView* view = new OrderOperationDetailsView(ui->allocationDetailsFrame);
		view->update(allocation, model);
		ui->allocationDetailsFrame->layout()->addWidget(view);
		allocation_details.reset(view);
	}
	else if(allocation.type == "J")
	{
		JourneyDetailsView* view = new JourneyDetailsView(ui->allocationDetailsFrame);
		view->update(allocation, model);
		ui->allocationDetailsFrame->layout()->addWidget(view);
		allocation_details.reset(view);
	}
	else if(allocation.type == "HP")
	{
		PickupDetailsView* view = new PickupDetailsView(ui->allocationDetailsFrame);
		view->update(allocation, model);
		ui->allocationDetailsFrame->layout()->addWidget(view);
		allocation_details.reset(view);
	}
	else if(allocation.type == "HR" || allocation.type == "HD")
	{
		HomeReturnDetailsView* view = new HomeReturnDetailsView(ui->allocationDetailsFrame);
		view->update(allocation, model);
		ui->allocationDetailsFrame->layout()->addWidget(view);
		allocation_details.reset(view);
	}
}

void AllocationView::setAllocationType(const Allocation &allocation)
{
	if(allocation.type == "O")
	{
		ui->allocationType->setText("Operation");
	}
	else if (allocation.type == "HP")
	{
		ui->allocationType->setText("Home pickup");
	}
	else if (allocation.type == "HR")
	{
		ui->allocationType->setText("Home return");
	}
	else if (allocation.type == "HD")
	{
		ui->allocationType->setText("Home drop");
	}
	else if (allocation.type == "TP")
	{
		ui->allocationType->setText("Time point");
	}
	else if (allocation.type == "J")
	{
		ui->allocationType->setText("Journey");
	}
	else if (allocation.type == "W")
	{
		ui->allocationType->setText("Waiting");
	}
	else ui->allocationType->setText("Unknown allocation");
}
