#ifndef PERFORMERVIEW_H
#define PERFORMERVIEW_H

#include <QWidget>

namespace Ui {
	class PerformerView;
}

class PerformerView : public QWidget
{
		Q_OBJECT

	signals:
		void performerRouteVisibilityChanged(int id, bool isVisible);
		void performerOrdersVisibilityChanged(int id, bool isVisible);

		void showScheduleRequest(int id);

	public:
		explicit PerformerView(QWidget *parent, int id);
		~PerformerView();

		void setName(const QString &name);
		void setColor(const QColor &color);


	private slots:
		void on_showRoutes_stateChanged(int arg1);

		void on_showPlannedOrders_stateChanged(int arg1);

		void on_showSchedule_clicked();

	private:
		Ui::PerformerView *ui;
		int id;
};

#endif // PERFORMERVIEW_H
