#ifndef MAINDATAMODEL_H
#define MAINDATAMODEL_H

#include <QDir>
#include "concepts.h"
#include <unordered_map>
#include <vector>

#include "savingssnapshot.h"
#include "schedulesnapshot.h"

class MainDataModel : public QObject
{
	Q_OBJECT

	public:
		MainDataModel();

		void setCurrentStep(long step_number);
		void load(QDir dump_folder);
		int getStepsCount();
		int getCurrentStep();
		long getCurrentSavingIndex() const;

		const std::unordered_map<QString, Order>& getOrders() const;
		const std::unordered_map<int, Performer>& getPerformers() const;
		const std::unordered_map<QString, GeoPoint>& getGeopoints();
		const std::unordered_map<QString, DC>& getDCs();

		const SavingsSnapshot& getSavingsSnapshot() const;
		const ScheduleSnapshot& getScheduleSnapshot() const;

		int getNextSuccessfulStep() const;
		int getPrevSuccessfulStep() const;

		QString getOrderReference(int index) const;

		void addSavingBreakPoint(const Saving &s);
		void removeSavingBreakPoint(const Saving &s);
		bool hasSavingBreakPoint(const Saving &s) const;
		void clearBreakPoints();

	signals:
		void breakPointReached() const;

	private:
		QDir dump_root;

		void loadOrders(QFile &file);
		void loadPerformers(QFile &file);
		void loadGeopoints(QFile &file);
		void loadDCs(QFile &file);
		void loadSavingIndex(QFile &file);

		void fillSavingSnapshotFiles();
		void fillScheduleSnapshotFiles();

		std::unordered_map<QString, Order> orders;
		std::unordered_map<int, Performer> performers;
		std::unordered_map<QString, GeoPoint> geopoints;
		std::unordered_map<QString, DC> dcs;
		std::vector<long> savings_index;

		SavingsSnapshot current_savings_snapshot;
		ScheduleSnapshot current_schedule_snapshot;

		std::vector<QString> saving_snapshot_filenames;
		std::vector<QString> schedule_snapshot_filenames;

		std::vector<QString> order_reference_index;

		std::vector<Saving> breakpoints;

		int current_step;
};

#endif // MAINDATAMODEL_H
