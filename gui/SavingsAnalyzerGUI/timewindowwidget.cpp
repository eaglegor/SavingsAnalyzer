#include "timewindowwidget.h"
#include "ui_timewindowwidget.h"
#include <QDateTime>

TimeWindowWidget::TimeWindowWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::TimeWindowWidget)
{
	ui->setupUi(this);
}

TimeWindowWidget::~TimeWindowWidget()
{
	delete ui;
}

void TimeWindowWidget::setBounds(qulonglong left, qulonglong right)
{
	QDateTime lb = QDateTime::fromMSecsSinceEpoch(left);
	QDateTime rb = QDateTime::fromMSecsSinceEpoch(right);

	ui->startTime->setText(lb.toString("dd.MM.yyyy hh:mm:ss"));
	ui->endTime->setText(rb.toString("dd.MM.yyyy hh:mm:ss"));
}
