#include "runview.h"
#include "scheduleview.h"
#include "ui_scheduleview.h"

ScheduleView::ScheduleView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::ScheduleView)
{
	ui->setupUi(this);
}

ScheduleView::~ScheduleView()
{
	delete ui;
}

void ScheduleView::update(const Schedule &schedule, const MainDataModel &model)
{
	const Performer &performer = model.getPerformers().at(schedule.performer_id);
	ui->performerName->setText(performer.resource_name);

	QString color_digits = QString::number(performer.color.red()) + ", " + QString::number(performer.color.green()) + ", " + QString::number(performer.color.blue());

	ui->performerInfo->setStyleSheet("background-color: rgb(" + color_digits + ");  color: rgb(255, 255, 255);");
	ui->mainFrame->setStyleSheet("background-color: rgb(" + color_digits + ", 10);");

	ui->runsWidget->clear();

	desiredHeight = 0;

	int index = 1;
	for(const Run &r : schedule.runs)
	{
		RunView* item = new RunView(index++, this);
		item->update(r, model);
		item->setStyleSheet(ui->mainFrame->styleSheet());
		item->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
		QListWidgetItem* lwitem = new QListWidgetItem();
		lwitem->setSizeHint(QSize(item->sizeHint().width(), item->getDesiredHeight()));
		ui->runsWidget->addItem(lwitem);
		ui->runsWidget->setItemWidget(lwitem, item);

		desiredHeight += item->getDesiredHeight() * 1.01;
	}

}

int ScheduleView::getDesiredHeight()
{
	return desiredHeight;
}
