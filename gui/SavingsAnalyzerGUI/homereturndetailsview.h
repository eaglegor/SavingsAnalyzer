#ifndef HOMERETURNDETAILSVIEW_H
#define HOMERETURNDETAILSVIEW_H

#include "concepts.h"
#include "maindatamodel.h"

#include <QWidget>

namespace Ui {
	class HomeReturnDetailsView;
}

class HomeReturnDetailsView : public QWidget
{
		Q_OBJECT

	public:
		explicit HomeReturnDetailsView(QWidget *parent = 0);
		~HomeReturnDetailsView();

		void update(const Allocation& allocation, const MainDataModel& model);

	private:
		Ui::HomeReturnDetailsView *ui;
};

#endif // HOMERETURNDETAILSVIEW_H
