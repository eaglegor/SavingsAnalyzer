#include "savingssnapshot.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDataStream>

SavingsSnapshot::SavingsSnapshot()
{

}

void SavingsSnapshot::load(QFile &file, std::unordered_map<QString, GeoPoint>& geopoints)
{
	savings.clear();

	QString val;
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	val = file.readAll();
	file.close();

	QJsonParseError err;

	QJsonDocument d = QJsonDocument::fromJson(val.toUtf8(), &err);
	QJsonObject root = d.object();

	QJsonValue val2 = root["savings"];

	QJsonArray performers_array = val2.toArray();

	for(const QJsonValueRef &v : performers_array)
	{
		Saving saving;
		saving.i= v.toObject()["i"].toInt();
		saving.j= v.toObject()["j"].toInt();
		saving.respStart = geopoints[v.toObject()["respFrom"].toString()];
		saving.respEnd = geopoints[v.toObject()["respTo"].toString()];
		saving.priority = v.toObject()["priority"].toInt();
		saving.value = v.toObject()["value"].toInt();
		saving.type = v.toObject()["type"].toString();
		savings.push_back(saving);
	}
}

const std::vector<Saving> &SavingsSnapshot::getSavings() const
{
	return savings;
}

