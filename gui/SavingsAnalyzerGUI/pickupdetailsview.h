#ifndef PICKUPDETAILSVIEW_H
#define PICKUPDETAILSVIEW_H

#include "maindatamodel.h"

#include <QWidget>

namespace Ui {
	class PickupDetailsView;
}

class PickupDetailsView : public QWidget
{
		Q_OBJECT

	public:
		explicit PickupDetailsView(QWidget *parent = 0);
		~PickupDetailsView();

		void update(const Allocation& allocation, const MainDataModel& model);

	private:
		Ui::PickupDetailsView *ui;
};

#endif // PICKUPDETAILSVIEW_H
