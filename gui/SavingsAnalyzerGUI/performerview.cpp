#include "performerview.h"
#include "ui_performerview.h"

PerformerView::PerformerView(QWidget *parent, int id) :
	QWidget(parent),
	id(id),
	ui(new Ui::PerformerView)
{
	ui->setupUi(this);
}

PerformerView::~PerformerView()
{
	delete ui;
}

void PerformerView::setName(const QString &name)
{
	ui->PerformerName->setText(name);
}

void PerformerView::setColor(const QColor &color)
{
	ui->frame->setStyleSheet(QString("background-color: ") + color.name() + QString(";") + QString("color: #FFFFFF"));
}

void PerformerView::on_showRoutes_stateChanged(int arg1)
{
	if(arg1 == Qt::Checked)
	{
		emit performerRouteVisibilityChanged(id, true);
	}
	else
	{
		emit performerRouteVisibilityChanged(id, false);
	}
}

void PerformerView::on_showPlannedOrders_stateChanged(int arg1)
{
	if(arg1 == Qt::Checked)
	{
		emit performerOrdersVisibilityChanged(id, true);
	}
	else
	{
		emit performerOrdersVisibilityChanged(id, false);
	}
}

void PerformerView::on_showSchedule_clicked()
{
	emit showScheduleRequest(id);
}
