#ifndef SCHEDULEVIEW_H
#define SCHEDULEVIEW_H

#include "concepts.h"
#include "maindatamodel.h"

#include <QWidget>

namespace Ui {
	class ScheduleView;
}

class ScheduleView : public QWidget
{
		Q_OBJECT

	public:
		explicit ScheduleView(QWidget *parent = 0);
		~ScheduleView();

		void update(const Schedule &schedule, const MainDataModel &model);

		int getDesiredHeight();

	private:
		Ui::ScheduleView *ui;
		int desiredHeight;
};

#endif // SCHEDULEVIEW_H
