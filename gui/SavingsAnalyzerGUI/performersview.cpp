#include "performersview.h"
#include "ui_performersview.h"

PerformersView::PerformersView(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::PerformersView)
{
	ui->setupUi(this);
}

void PerformersView::update(const MainDataModel &model)
{
	const std::unordered_map<int, Performer>& performers = model.getPerformers();

	checkboxes.clear();
	items.clear();

	for(PerformerView* view : checkboxes)
	{
		delete view;
	}
	for(QListWidgetItem* item : items)
	{
		delete item;
	}

	ui->listWidget->clear();

	for(auto &iter : performers)
	{
		PerformerView* item = new PerformerView(this, iter.second.id);
		item->setColor(iter.second.color);
		item->setName(iter.second.resource_name);
		QObject::connect(item, SIGNAL(performerRouteVisibilityChanged(int,bool)), this, SLOT(onPerformerRouteVisibilityChanged(int,bool)));
		QObject::connect(item, SIGNAL(performerOrdersVisibilityChanged(int,bool)), this, SLOT(onPerformerOrdersVisibilityChanged(int,bool)));
		QObject::connect(item, SIGNAL(showScheduleRequest(int)), this, SLOT(onShowScheduleRequest(int)));
		QListWidgetItem* lwitem = new QListWidgetItem();
		lwitem->setSizeHint(item->sizeHint());
		ui->listWidget->addItem(lwitem);
		ui->listWidget->setItemWidget(lwitem, item);
	}
}

PerformersView::~PerformersView()
{
	delete ui;

	for(PerformerView* view : checkboxes)
	{
		delete view;
	}
	for(QListWidgetItem* item : items)
	{
		delete item;
	}
}
