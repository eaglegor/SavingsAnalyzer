#include "allocationview.h"
#include "mainwindow.h"
#include "runview.h"
#include "scheduleview.h"
#include "ui_mainwindow.h"

#include <iostream>

#include <QWebView>
#include <QWebFrame>
#include <QFileDialog>
#include <QTimer>
#include "savingsviewtablemodel.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	savings_table_view(nullptr),
	savings_table_model(new SavingsViewTableModel(model)),
	performersView(nullptr),
	schedules_view(nullptr),
	drawCurrentSaving(false)
{
	ui->setupUi(this);
	timer = new QTimer();

	QObject::connect(&model, SIGNAL(breakPointReached()), this, SLOT(onBreakPointReached()));
}

MainWindow::~MainWindow()
{
	delete timer;
	delete ui;
	if(savings_table_view) delete savings_table_view;
	if(savings_table_model) delete savings_table_model;
}

void MainWindow::on_fitMapButton_clicked()
{
	ui->mapView->page()->mainFrame()->evaluateJavaScript(QString("fitMap();"));
}

void MainWindow::on_actionLoadDump_triggered()
{
	QFileDialog dialog(this);
	dialog.setFileMode(QFileDialog::DirectoryOnly);
	dialog.setWindowTitle("Choose dump directory");
	dialog.setModal(true);

	if(!dialog.exec())
	{
		return;
	}


	if(dialog.selectedFiles().empty()) return;

	QString dirName = *dialog.selectedFiles().begin();

	model.load(QDir(dirName));

	schedule_views.resize(model.getPerformers().size(), nullptr);

	ui->mapView->page()->mainFrame()->evaluateJavaScript("clearOrders(); clearMap(); clearOrderHighlights();");

	ui->currentStepSlider->setMinimum(0);
	ui->currentStepSlider->setMaximum(model.getStepsCount() - 1);

	ui->currentStepSlider->setValue(0);

	drawOrders();

	setCurrentStep(0);

	ui->mapView->page()->mainFrame()->evaluateJavaScript(QString("fitMap();"));

	if(savings_table_model) delete savings_table_model;
	savings_table_model = new SavingsViewTableModel(model);

	if(savings_table_view) savings_table_view->setTableModel(savings_table_model);

	if(performersView) performersView->update(model);
}

void MainWindow::on_nextStepButton_clicked()
{
	ui->currentStepSlider->setValue(ui->currentStepSlider->value() + 1);
}

void MainWindow::on_prevStepButton_clicked()
{
	ui->currentStepSlider->setValue(ui->currentStepSlider->value() - 1);
}

void MainWindow::on_currentStepSlider_valueChanged(int value)
{
	setCurrentStep(value);
}

void MainWindow::drawRESPs()
{
	QString query;
	{
		const std::unordered_map<QString, DC> &dcs = model.getDCs();

		for(auto& iter : dcs)
		{
			query += QString("addDCMarker(\"") + QString(iter.second.name) + QString("\",") + QString::number(iter.second.location.latitude) + QString(",") + QString::number(iter.second.location.longitude) + QString(");");
		}
	}
	{
		const std::unordered_map<int, Performer> &performers = model.getPerformers();

		for(auto& iter : performers)
		{
			if(!(iter.second.dcPoint == iter.second.startPoint))
			{
				query += QString("addStartPointMarker(\"") + QString(iter.second.resource_name) + QString("\",") + QString::number(iter.second.startPoint.latitude) + QString(",") + QString::number(iter.second.startPoint.longitude) + QString(");");
			}
			if(!(iter.second.dcPoint == iter.second.endPoint))
			{
				query += QString("addEndPointMarker(\"") + QString(iter.second.resource_name) + QString("\",") + QString::number(iter.second.endPoint.latitude) + QString(",") + QString::number(iter.second.endPoint.longitude) + QString(");");
			}
		}
	}
	ui->mapView->page()->mainFrame()->evaluateJavaScript(query);
}

void MainWindow::drawOrders()
{
	const std::unordered_map<QString, Order> &orders = model.getOrders();

	QString query;

	for(auto& iter : orders)
	{
		query += QString("addOrderMarker(\"") + QString(iter.second.reference) + QString("\",") + QString::number(iter.second.location.latitude) + QString(",") + QString::number(iter.second.location.longitude) + QString(");");
	}

	query += "fitMap();";

	ui->mapView->page()->mainFrame()->evaluateJavaScript(query);
}

void MainWindow::drawJourneys()
{
	QString query;
	for(const Schedule &schedule : model.getScheduleSnapshot().getSchedules())
	{
		if(route_disabled_performers.find(schedule.performer_id) != route_disabled_performers.end()) continue;
		for(const Run& run : schedule.runs)
		{
			for(const Allocation &allocation: run.allocations)
			{
				query += QString("addLine(\"" + model.getPerformers().at(schedule.performer_id).color.name() + "\",") + QString::number(allocation.startPoint.latitude) + QString(",") + QString::number(allocation.startPoint.longitude) + QString(",") + QString::number(allocation.endPoint.latitude) + QString(",") + QString::number(allocation.endPoint.longitude) + QString(");");
			}
		}
	}
	ui->mapView->page()->mainFrame()->evaluateJavaScript(query);
}

void MainWindow::updatePlannedJobs()
{
	QString query;
	for(const Schedule &schedule : model.getScheduleSnapshot().getSchedules())
	{
		bool disabled_performer = (orders_disabled_performers.find(schedule.performer_id) != orders_disabled_performers.end());
		for(const Run& run : schedule.runs)
		{
			int index = 1;
			for(const Allocation &allocation: run.allocations)
			{
				if(allocation.type == "O")
				{
					if(disabled_performer)
					{
						query += QString("hideOrder(\"" + allocation.order_reference + "\");");
					}
					else
					{
						query += QString("setPlanned(\"") + allocation.order_reference + QString("\", ") + QString::number(index++) + QString(", \"") +  model.getPerformers().at(schedule.performer_id).color.name() + QString("\");");
					}
				}
			}
		}
	}
	ui->mapView->page()->mainFrame()->evaluateJavaScript(query);
}

QString MainWindow::formatSavingString(const int index, const int count, const Saving &s)
{
	return QString("[") + QString::number(index) + QString("/") + QString::number(count) + QString("] ") + QString::number(s.i) + " -> " + QString::number(s.j) + \
			QString(" P: ") + QString::number(s.priority) + QString(" = ") + QString::number(s.value);
}

void MainWindow::setCurrentStep(int step)
{
	model.setCurrentStep(step);

	ui->currentStepLabel->setText(QString::number(step));

	ui->mapView->page()->mainFrame()->evaluateJavaScript("clearMap(); resetPlanned(); clearOrderHighlights();");

	ui->currentSavingLabel->setText(formatSavingString(model.getCurrentSavingIndex(), model.getSavingsSnapshot().getSavings().size(), model.getSavingsSnapshot().getSavings()[model.getCurrentSavingIndex()]));

	ui->plannedJobsLabel->setText(QString::number(model.getScheduleSnapshot().getPlannedJobs()) + QString("/") + QString::number(model.getOrders().size()));
	ui->usedVehiclesLabel->setText(QString::number(model.getScheduleSnapshot().getUsedVehicles()) + QString("/") + QString::number(model.getPerformers().size()));

	drawRESPs();
	drawJourneys();

	updatePlannedJobs();

	const Saving &s = model.getSavingsSnapshot().getSavings()[model.getCurrentSavingIndex()];

	if(drawCurrentSaving)
	{
		QString highlightQuery;
		highlightQuery += QString("highlightOrder(\"") + QString(model.getOrderReference(s.i)) + QString("\",\"#FF0000\");");
		highlightQuery += QString("highlightOrder(\"") + QString(model.getOrderReference(s.j)) + QString("\",\"#0000FF\");");
		ui->mapView->page()->mainFrame()->evaluateJavaScript(highlightQuery);
	}

	if(savings_table_view) savings_table_view->setCurrentSaving(model.getCurrentSavingIndex());

	if(!schedules_view) {
		schedules_view = new SchedulesView(this);
		schedules_view->setWindowFlags(Qt::Window);
	}

	schedules_view->update(model);

	for(int i = 0; i < model.getPerformers().size(); ++i)
	{
		if(schedule_views[i]) schedule_views[i]->update(model.getScheduleSnapshot().getSchedules()[i], model);
	}
}

void MainWindow::on_nextSuccStepButton_clicked()
{
	ui->currentStepSlider->setValue(model.getNextSuccessfulStep());
}

void MainWindow::on_prevSuccStepButton_clicked()
{
	ui->currentStepSlider->setValue(model.getPrevSuccessfulStep());
}

void MainWindow::on_actionShow_savings_table_triggered()
{
	if(savings_table_view == nullptr)
	{
		savings_table_view = new SavingsTableView(this);
	}
	savings_table_view->setTableModel(savings_table_model);
	savings_table_view->setWindowFlags(Qt::Window);
	savings_table_view->setCurrentSaving(model.getCurrentSavingIndex());
	savings_table_view->show();
}

void MainWindow::nextFrame()
{
	on_nextSuccStepButton_clicked();
}

void MainWindow::on_actionShow_vehicles_triggered()
{
	if(!performersView) performersView = new PerformersView(this);
	performersView->update(model);
	performersView->setWindowFlags(Qt::Window);
	performersView->show();
	QObject::connect(performersView, SIGNAL(performerRouteVisibilityChanged(int,bool)), this, SLOT(onPerformerRoutesVisibilityChanged(int,bool)));
	QObject::connect(performersView, SIGNAL(performerOrdersVisibilityChanged(int,bool)), this, SLOT(onPerformerOrdersVisibilityChanged(int,bool)));
	QObject::connect(performersView, SIGNAL(showScheduleRequest(int)), this, SLOT(onShowScheduleRequest(int)));
}

void MainWindow::onPerformerRoutesVisibilityChanged(int id, bool isVisible)
{
	if(isVisible)
	{
		route_disabled_performers.erase(id);
	}
	else
	{
		route_disabled_performers.emplace(id);
	}
	setCurrentStep(model.getCurrentStep());
}

void MainWindow::onPerformerOrdersVisibilityChanged(int id, bool isVisible)
{
	if(isVisible)
	{
		orders_disabled_performers.erase(id);
	}
	else
	{
		orders_disabled_performers.emplace(id);
	}
	setCurrentStep(model.getCurrentStep());
}


void MainWindow::on_drawCurrentSaving_stateChanged(int arg1)
{
	if(arg1 == Qt::Checked)
	{
		drawCurrentSaving = true;
	}
	else
	{
		drawCurrentSaving = false;
	}
	setCurrentStep(model.getCurrentStep());
}

void MainWindow::on_autoplay_stateChanged(int arg1)
{
	if(arg1 == Qt::Checked)
	{
		QObject::connect(timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
		timer->start(1000);
	}
	else
	{
		timer->stop();
		QObject::disconnect(timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
	}
}

void MainWindow::on_actionShow_schedules_list_triggered()
{
	if(schedules_view) schedules_view->show();
}

void MainWindow::onShowScheduleRequest(int id)
{
	if(!schedule_views[id])
	{
		schedule_views[id] = new ScheduleView(this);
		schedule_views[id]->setWindowFlags(Qt::Window);
	}
	schedule_views[id]->update(model.getScheduleSnapshot().getSchedules()[id], model);
	schedule_views[id]->show();
}

void MainWindow::onBreakPointReached()
{
	if(ui->autoplay->isChecked())
	{
		on_autoplay_stateChanged(Qt::Unchecked);
		ui->autoplay->setChecked(false);
	}
}
