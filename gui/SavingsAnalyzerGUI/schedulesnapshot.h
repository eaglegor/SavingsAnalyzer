#ifndef SCHEDULESNAPSHOT_H
#define SCHEDULESNAPSHOT_H

#include <QFile>
#include "concepts.h"
#include <vector>
#include <unordered_map>

class ScheduleSnapshot
{
	public:
		ScheduleSnapshot();

		void load(QFile &file, std::unordered_map<QString, GeoPoint> &geopoints);

		size_t getPlannedJobs() const;
		size_t getUsedVehicles() const;
		const std::vector<Schedule>& getSchedules() const;

	private:
		std::vector<Schedule> schedules;
		size_t planned_jobs;
		size_t used_vehicles;
};

#endif // SCHEDULESNAPSHOT_H
