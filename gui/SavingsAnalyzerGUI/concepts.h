#ifndef CONCEPTS_H
#define CONCEPTS_H

#include <string>
#include <vector>
#include <QString>
#include <QColor>

struct GeoPoint
{
	QString name;
	float latitude;
	float longitude;
	QString address;

	bool operator==(const GeoPoint &rhs) const
	{
		return name == rhs.name;
	}
};

struct DC
{
	QString  name;
	GeoPoint location;
};

struct Performer
{
	size_t id;
	QString resource_name;
	QString  dc_name;
	qulonglong start_time;
	qulonglong end_time;
	long driving_time_limit;
	long duty_time_limit;
	std::vector<float> capacity;
	std::vector<QString > attributes;

	QColor color;

	GeoPoint startPoint;
	GeoPoint dcPoint;
	GeoPoint endPoint;
};

struct Order
{
	QString  reference;
	QString  dc_name;
	qulonglong window_start;
	qulonglong window_end;
	std::vector<float> dimensions;
	int priority;
	std::vector<QString > attributes;
	QString  type;
	GeoPoint location;
};

struct BreakTime
{
	qulonglong startTime;
	qulonglong endTime;
};

struct Allocation
{
	QString  order_reference;
	QString  type;
	qulonglong startTime;
	qulonglong endTime;
	GeoPoint startPoint;
	GeoPoint endPoint;
	std::vector<BreakTime> breaks;
};

struct Run
{
	qulonglong start_time;
	qulonglong end_time;
	GeoPoint startPoint;
	GeoPoint endPoint;
	std::vector<Allocation> allocations;
};

struct Schedule
{
	size_t performer_id;
	std::vector<Run> runs;
};

struct Saving
{
	size_t i;
	size_t j;
	GeoPoint respStart;
	GeoPoint respEnd;
	int priority;
	long value;
	QString type;

	bool operator==(const Saving& rhs) const
	{
		return i == rhs.i && j == rhs.j && type == rhs.type;
	}
};

namespace std
{
	template<> struct hash<QString>
	{
		int operator()(const QString& str) const
		{
			return std::hash<std::string>()(str.toStdString());
		}
	};
}


#endif // CONCEPTS_H

